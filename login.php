<?php
header('Content-Type: text/html; charset=UTF-8');

session_start();


if (!empty($_SESSION['log'])) {
  header('Location: ./');
}
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
?>

  <head>
    <title>Login</title>
    <meta charset="UTF-8" />
    <link rel="stylesheet" href="main.css">
    <style>
      .error {
        border: 2px solid red;
      }
    </style>
  </head>

  <form class="decor" action="" method="post">
    <div class="form-left-decoration"></div>
    <div class="form-right-decoration"></div>
    <div class="circle">
      <div id="heart"></div>
      <div id="infinity"></div>
    </div>

    <div class="form-inner">
      <h3>Добро пожаловать</h3>
      <?php if (!empty($_COOKIE['error'])) {
        setcookie('error', '', 100000);
        print('<div id="error"><strong>неверный логин или пароль!</strong></div>');
      }

      ?>
      <div <?php if (!empty($_COOKIE['error'])) {
              setcookie('error', '', 100000);
              print 'class="error"';
            }
            ?>>
        <input type="text" placeholder="Login" name="login" value="">
        <input type="password"  maxlength="8" placeholder="password" name="pass" value=""></div>
      <input type="submit" value="Войти">
      <input type="button" onclick="window.location= './';" value="Назад">
    </div>
  </form>

<?php
} else {
  $user = 'u16364';
  $passw = '6346523';
  $db = new PDO(
    'mysql:host=localhost;dbname=u16364',
    $user,
    $passw,
    array(PDO::ATTR_PERSISTENT => true)
  );
  $_SESSION['log'] = '~';
  $stmt = $db->prepare('SELECT id, login FROM application WHERE login = ? AND password = ?');
  $stmt->execute([$_POST['login'], md5($_POST['pass'])]);
  while ($row = $stmt->fetch(PDO::FETCH_LAZY)) {
    $_SESSION['log'] = strip_tags($row['login']);
    $_SESSION['uid'] = $row['id'];
    header('Location: ./');
  }

  if ($_SESSION['log'] == '~') {
    setcookie('error', '1',  time() + 24 * 60 * 60);
    session_destroy();
    header('Location: login.php');
  }
}